
\documentclass[11pt,ngerman,colorbacktitle,accentcolor=tud4b]{tudreport}

\usepackage{ifluatex}
\ifluatex
	\usepackage[utf8]{luainputenc}
\else
	\usepackage[utf8]{inputenc}
\fi
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{fancyvrb}
\usepackage{color}
\usepackage{caption}
% \usepackage{tikz}
\usepackage{amsmath}
\usepackage[per-mode=symbol]{siunitx}
\usepackage{microtype}
\newcommand{\hmmax}{0}
\newcommand{\bmmax}{2}
\usepackage{physmak}
\usepackage{fixltx2e}
\usepackage[square,numbers]{natbib}
\usepackage{tabularx}
\usepackage{multirow}
\newcolumntype{L}[1]{>{\raggedright\arraybackslash}p{#1}} % linksbündig mit Breitenangabe
\newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1}} % zentriert mit Breitenangabe
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash}p{#1}} % rechtsbündig mit Breitenangabe



\input{Qcircuit}



\setlength{\parindent}{0pt}
\renewcommand{\arraystretch}{1.8}

\sisetup{%
	list-final-separator = {\text{ und }},
	range-phrase = { \text{bis} },
	per-mode = reciprocal,
	separate-uncertainty,
	round-mode = places,
	round-precision = 2,
	inter-unit-product = \ensuremath{{}\cdot{}}
}

\DeclareSIUnit[number-unit-product = {}]
	\clight{c}

\begin{document}
	
\title{Quantencomputer - Realisierung von Gattern}
\subtitle{Seminar Quanteninformation SS14 - Ausarbeitung von Ludwig Kunz}
	
\maketitle
\tableofcontents

\chapter{Theoretische Grundlagen}

\section{Qubits}

Einer der elementaren Bausteine eines Quantencomputers ist das Qubit. Dort wird die Information gespeichert und Operationen zur Informationsverarbeitung auf ihnen ausgeführt. Ein Qubit ist ein quantenmechanisches Zwei-Niveau-System mit den Basiszuständen $\ket{0}$ und $\ket{1}$ aus dem Hilbertraum $\hilbert$, wobei die Basiszustände die logische $0$ und $1$ des klassischen Bits vergleichbar sind. Im Unterschied zum klassischen Bit können aber auch beliebige Superpositionen der beiden Basiszustände gespeichert werden. Ein allgemeiner Zustand lässt sich demnach schreiben als
\begin{align}
	\ket{\psi} = \sin \left ( \frac{\theta}{2} \right ) \ket{0} + e^{i \varphi} \cos \left ( \frac{\theta}{2} \right ) \ket{1} \; .
\end{align}
Dies lässt sich auch graphisch auf der sogenannten Blochkugel in \reffig{fig:bloch} darstellen. Die Basiszustände liegen dabei auf den Polen der Kugel und alle möglichen Superpositionen sind auf der Kugeloberfläche zu finden, da der Zustand normiert ist und somit einen festen Radius in dieser Darstellung besitzt. Globale Phasen $\ket{\psi '} = e^{i \xi} \ket{\psi}$ sind dabei nicht beobachtbar, da sie sich bei einer Messung herausmitteln. \\
Da ein Qubit als ein Zwei-Niveau-System definiert ist, gibt es sehr viele Realisationsmöglichkeiten. Ein Beispiel dafür ist der Kernspin oder der elektrische Zustand eines Atoms. Aber auch nicht massive Systeme wie die Polarisation des Lichts sind als Qubit möglich.

\begin{figure}[h!tp]
	\centering
\includegraphics[height=.3\textheight]{Bilder/bloch.png}
\caption{Darstellung der Blochkugel \cite{bloch-wiki}}
\label{fig:bloch}
\end{figure}

\section{Quantengatter}

Um Informationen in einem Computer verarbeiten zu können, ist es nötig Operationen auf den Qubits durchführen zu können. Diese elementaren Operationen werden Quantengatter genannt. Dabei unterscheidet man die Gatter nach der Anzahl der Qubits, auf die sie wirken. Für das 2-Qubit-Gatter ist beispielsweise der Transistor das klassische Analogon. \\
Während bei einem klassischen Computer Gatter Schaltkreise darstellen, sind Quantengatter bei einem Quantencomputer zeitliche Operationen, die auf das Qubit wirken. \\
Die einzige mathematische Bedingung, die wir an ein solches Quantengatter stellen müssen, ist die Unitarität, d.h. für die unitäre Transformation $U$ gilt
\begin{align}
	\abs{U \psi}^2 = \abs{\psi}^2 \; .
\end{align}
Aus der Unitarität folgt direkt die Umkehrbarkeit der Transformation. Eine weitere praxisrelevante Bedingung an ein Quantengatter ist die Skalierbarkeit des Systems. Skalierbarkeit bedeutet, dass wir ein 2-Qubit-Gatter welches mit zwei Qubits realisiert wurde, auch bei Anordnungen mit mehr Qubits funktionieren muss.\\
Desweiteren muss darauf geachtet werden, dass Dekohärenz des Systems nur auf genügend großen Zeitskalen stattfindet. Denn es ist notwendig, dass man alle Operationen inklusive der Messung ausführen kann, bevor der Zustand des Qubits durch Umwelteinflüsse oder spontane Prozesse zerfällt und keine Informationsverarbeitung damit möglich ist. \\
Der Typ eines Quantengatters ist durch seine Wirkung auf die Basiszustände $\ket{0}$ und $\ket{1}$, d.h. die Wahrheitstafel bzw. Matrix, bestimmt. Im \reftab{tab:types} werden einige wichtige Beispiele für Typen von Quantengattern aufgeführt. \\
Das NOT-Gatter ist ein 1-Qubit-Gatter und invertiert den Zustand des Qubits. Auf der Bloch-Kugel entspricht dies einer Drehung vom Südpol zum Nordpol bzw. vom Nordpol zum Südpol.
Das Hadamard-Gatter dreht den Zustand auf der Bloch-Kugel um $90^\circ$.
Ein weiteres wichtiges 1-Qubit-Gatter ist das Phasengatter. Es verursacht eine Phasenänderung des Zustands um $e^{\frac{i \pi}{2^k}}$.
Das wichtigste Beispiel für ein 2-Qubit-Gatter ist das sogenannte CNOT-Gatter oder controlled NOT-Gatter. Es bewirkt eine XOR-Verknüpfung der beiden Qubits (Control und Target) und speichert das Ergebnis im Target-Qubit. Die Schreibweise $\ket{0,1}$ ist dabei als $\ket{0}_{\text{control}} \ket{1}_{\text{target}}$ zu verstehen.

\begin{table}[h!tp]
	\centering
	\begin{tabular}{|c|C{4cm}|c|c|}
		\hline 
		 & Wirkung & Symbol & Matrix \\ \hline
		 \multirow{2}{*}{NOT-Gatter} & $\ket{0} \rightarrow \ket{1}$ & \multirow{2}{*}{$\Qcircuit @C=1em @R=.7em { & \gate{X} & \qw}$} & \multirow{2}{*}{$\begin{pmatrix}	0 & 1 \\ 1 & 0 \end{pmatrix}$} \\
		 & $\ket{1} \rightarrow \ket{0}$ &  &  \\ \hline
 		 \multirow{2}{*}{Hadamard-Gatter} & $\ket{0} \rightarrow \frac{1}{\sqrt{2}} (\ket{0} + \ket{1})$ & \multirow{2}{*}{$\Qcircuit @C=1em @R=.7em {& \gate{H} & \qw}$} & \multirow{2}{*}{$\frac{1}{\sqrt{2}} \begin{pmatrix} 1 & 1 \\	1 & -1 \end{pmatrix}$} \\ 
		 & $\ket{1} \rightarrow \frac{1}{\sqrt{2}} (\ket{0} - \ket{1})$ & & \\ \hline
		 Phasengatter & $\ket{1} \rightarrow e^{\frac{i \pi}{2^k}} \ket{1}$ & $\Qcircuit @C=1em @R=.7em {& \gate{\varphi} & \qw}$ & $\begin{pmatrix}1 & 0 \\ 0 & e^{\frac{i \pi}{2^k}}\end{pmatrix}$ \\ \hline
		 \multirow{4}{*}{CNOT-Gatter} & $\ket{0,0} \rightarrow \ket{0,0}$ & \multirow{4}{*}{$\Qcircuit {& \ctrl{1} \qw & \qw \\ & \targ & \qw}$} & \multirow{4}{*}{\hfill$\begin{pmatrix} 1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\0 & 0 & 0 & 1 \\0 & 0 & 1 & 0 \end{pmatrix}$} \\ 
		 & $\ket{0,1} \rightarrow \ket{0,1}$ & & \\ 
		 & $\ket{1,0} \rightarrow \ket{1,1}$ & & \\ 
		 & $\ket{1,1} \rightarrow \ket{1,0}$ & & \\ \hline
	\end{tabular}
	\caption{Verschiedene Typen von Quantengattern}
	\label{tab:types}
\end{table}

Um zu verstehen, warum diese 4 Beispiele gewählt wurden, muss man sich zunächst das Konzept der Universalität ansehen. Aus praktischen Gründen wäre es wünschenswert, dass man einen gewissen Satz von Gattern realisiert und mit Hilfe von diesen Bausteinen sich alle möglichen anderen Gattern zusammenbauen lassen. Bei einem klassischen Computer ist es zum Beispiel möglich mit Hilfe des NAND-Gatters alle anderen Gatter zu bauen. \\
Dies lässt sich auch für die Quantengatter beweisen. Man benötigt einen vollständigen Satz von 1-Qubit-Gattern (z.B. Identität, NOT, Hadamard und $\frac{\pi}{8}$-Phasengatter) und zusätzlich ein einziges 2-Qubit-Gatter (z.B. das CNOT-Gatter). Mit diesem Satz von grundlegenden Gattern, lassen sich alle anderen Gatter (d.h. auch 3-Qubit, 4-Qubit-Gatter, etc) realisieren. Dies mag zwar nicht die Variante mit der größten Performance sein, aber es lässt sich so exakt und nicht nur näherungsweise realisieren. Der Beweis für die Universalität soll hier nicht gezeigt werden. Er lässt sich unter anderem in \cite{Nielsen2007Quantum-Computa} nachlesen.

\section{Ionenfalle/Optisches Gitter}

Ein weiteres grundlegendes Element eines Quantencomputers ist eine geeignete experimentelle Realisierungstechnik. Hierbei wollen wir uns auf die Ionenfalle bzw. das optische Gitter beschränken. Dort werden Atome in Potentialfallen gefangen und als Qubits verwendet. Man unterscheidet dabei im wesentlichen zwei Typen von Fallen, die magnetische Falle und die optische Falle, die mit Lasern gebaut wird. Während die magnetische Falle wesentlich stabiler ist, lässt sie sich dafür aber nicht flexibel einstellen und verändern. Optische Fallen hingegen sind zwar nicht so stabil wie magnetische Fallen, sind dafür aber sehr flexibel. Die Potentialtöpfe lassen sich dabei verschieben und sind auch in ihrer Breite und Tiefe variierbar. Für genauere Details sei an dieser Stelle auf den Vortrag zu der Ionenfalle bzw. dem optischen Gitter verwiesen.

% \begin{figure}[h!tp]
% 	\centering
% 	\includegraphics[width=.6\textwidth]{Bilder/2D-gitter.pdf}
% 	\caption{Ein mögliches optisches Gitter. An den weißen und schwarzen Punkten lässt sich jeweils ein Atom fangen. \cite{Deutsch2000Quantum-Computi}}
% 	\label{fig:gitter}
% \end{figure}

\chapter{Realisierungen von Quantengattern}

Nun sollen verschiedene Realisierungen von Quantengatter besprochen werden. Dabei werden verschiedene Arten von Wechselwirkungen dazu verwendet, ein CNOT-Gatter zu bauen.

\section{Cirac-Zoller-Gatter für Ionen}
\label{sec:cirac}

\subsection{Idee}

Das Cirac-Zoller-Gatter basiert auf einem Vorschlag von Cirac und Zoller von 1995 \cite{Cirac1995Quantum-Comp}, der schematisch in \reffig{fig:ciraczoller} abgebildet ist.
Der grundlegende Aufbau besteht aus einer Kette von $N$ Ionen, die jeweils einzeln mit einem Laser angesteuert werden können. Zwei elektrische Zustände der Ionen, der Grundzustand $\ket{g}$ und ein angeregter Zustand $\ket{e}$, werden als Zustände $\ket{0}$ und $\ket{1}$ verwendet. Als eine Art Zwischenspeicher in dem Informationen gespeichert werden können, wird eine gemeinsame Vibrationsmode aller $N$ Ionen verwendet. Dieser Speicher ist notwendig, da die Informationen zunächst nur lokal auf einem Ion gespeichert sind und wir keine Möglichkeit haben, auf die Information von zwei Qubits gleichzeitig zuzugreifen. \\
Die Idee ist nun, dass man den Zustand von Ion I auf die Vibrationsmode überträgt, damit man dann abhängig von dem genauen Zustand der Mode den Zustand von Ion II verändern kann. Danach überträgt man den Zustand von der Vibrationsmode wieder auf Ion I und man hat die CNOT-Operation durchgeführt. Die genauen Operationen und Details dieses Vorgangs werden wir nun anhand einer Realisierung von Schmidt et al. \cite{Schmidt-Kaiser2003Realization-of-} im Folgenden genauer betrachten.
\begin{figure}[h!tp]
			\centering
			\includegraphics[width=.65\textwidth]{Bilder/Cirac_Zoller_Aufbau.pdf}
			\caption{Schematische Darstellung der Funktionsweise \cite{Cirac1995Quantum-Comp}}
			\label{fig:ciraczoller}
\end{figure}

\subsection{Termschema von Ca+}

Zunächst müssen wir uns das Termschema von Ca+ in \reffig{fig:ca} genauer ansehen.
%
\begin{figure}[h!tp]
\centering
\includegraphics[width=.55\textwidth]{Bilder/termschema.pdf}
\caption{Termschema von Ca+ \cite{vortragZoller}}
\label{fig:ca}
\end{figure}
%
Die Basiszustände $\ket{0}$ und $\ket{1}$ werden mit den Zuständen $S_{1/2}$ und $D_{5/2}$ codiert und im folgenden auch $\ket{S}$ und $\ket{D}$ genannt. Der Zustand $D_{5/2}$ ist dabei metastabil mit einer Lebenszeit von $\tau = \SI{1}{\second}$, was für unsere Zwecke lange genug ist, da die Operationen deutlich schneller ausgeführt werden können. Der Übergang von $P_{1/2}$ zu $S_{1/2}$ wird zum optischen Kühlen, sowie zum Messen des Qubit-Zustandes verwendet. Dazu strahlt man Licht der Wellenlänge $\SI{397}{\nano \meter}$ ein. Befindet sich nun das Ion in Zustand $S_{1/2}$, d.h. $\ket{0}$, wird der Übergang angeregt und das Ion leuchtet. Im Zustand $D_{5/1}$, d.h. $\ket{1}$, wird kein Übergang angeregt und das Ion bleibt dunkel. Die anderen Übergänge bzw. Zustände werden für praktische Zwecke verwendet, wie bespielsweise zum Verstecken eines Atomes. Allerdings zur grundlegenden Realisierung nicht notwendig und werden daher hier nicht weiter betrachtet. \\
%
Abgesehen von den elektrischen Zuständen in denen das Qubit codiert wird, benötigen wir noch die kinetischen Zustände der Ionenkette, also die Vibrationsmode. Da die Ionen in Potentialen gefangen sind, können wie bei einem harmonischen Oszillator (\reffig{fig:hmo}) Schwingungsmoden angeregt werden. Durch die Anordnung in einer Kette schwingen dabei alle Ionen. Diese Moden stellen ein weiteres quantenmechanisches System mit einer Quantenzahl $n$ dar. Insgesamt besitzen wir also ein sogenanntes dressed System mit zwei Quantenzahlen, dem elektrischen Zustand und dem Zustand der Vibrationsmode. Der Zustand der Vibrationsmode wird auch als ein Seitenband bezeichnet, wie in \reffig{fig:seitenband}. Wie man zwischen Seitenbändern wechselt, sehen wir im Folgenden.
\begin{figure}[h!tp]
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[width=.6\textwidth]{Bilder/hmo.pdf}
	\caption{Harmonischer Oszillator \cite{vortragZoller}}
	\label{fig:hmo}
\end{minipage}
%
\begin{minipage}{.5\textwidth}
	\centering
	\includegraphics[width=\textwidth]{Bilder/seitenband.pdf}
	\captionof{figure}{Schema der Seitenbänder von Ca+ \cite{vortragZoller}}
	\label{fig:seitenband}
\end{minipage}
\end{figure}

\subsection{Pulssequenz}

Nun schauen wir uns an, mit welchen Pulsen wir das CNOT-Gatter realisieren wollen. Die gewünschte Wirkung des CNOT-Gatters mit Hilfe der Zustände $\ket{S}$ und $\ket{D}$ sieht dabei wie folgt aus:
\begin{align}
	\ket{S,S} &\rightarrow \ket{S,S} \\ \nonumber
	\ket{S,D} &\rightarrow \ket{S,D} \\ \nonumber
	\ket{D,S} &\rightarrow \ket{D,D} \\ \nonumber
	\ket{D,D} &\rightarrow \ket{D,S}
\end{align}
Die Umsetzung durch Pulse ist die Folgende:
\begin{align}
\Qcircuit @C=1.em @R=1em {
\lstick{I} & \qw & \multigate{1}{\text{SWAP}} & \qw 	& \qw  & \qw & \multigate{1}{\text{SWAP}^{-1}} & \qw \\
\lstick{V} & \qw & \ghost{\text{SWAP}}        & \qw 	&	\ctrl{1}  & \qw & \ghost{\text{SWAP}^{-1}} & \qw \\
\lstick{II} & \qw &	\qw	& \qw & \targ	& \qw & \qw & \qw \\
} 
\end{align}
Die Vibrationsmode V stellt dabei ein eigenes drittes Qubit dar, unserem Zwischenspeicher. Zunächst übertragen wir mit der SWAP-Operation den Zustand von Ion I (Control-Qubit) auf die Vibrationsmode V. Dann invertieren wir den Zustand des Ions II (Target-Qubit) abhängig vom Zustand der Vibrationsmode. Dies stellt die eigentliche CNOT-Operation dar. Danach übertragen wir durch $\text{SWAP}^{-1}$ den Zustand von V wieder auf Ion I.

\subsubsection{SWAP (Control $\rightarrow$ Vibrationsmode)}

Die SWAP-Operation realisieren wir durch einem $\pi$-Puls auf dem blauem Seitenband, wie in \reffig{fig:swap}. Die Zustände $\ket{S,0}$ und $\ket{D,1}$ bilden ein Zwei-Zustands-System, das Vakuum-Rabi-Oszillation mit der Frequenz $\Omega$ vollführt. Strahlen wir nun einen Laserpuls mit resonanter Wellenlänge für eine Zeit von $t = \frac{\pi}{\Omega}$, d.h. einen $\pi$-Puls, ein, wechselt das System von $\ket{S,0}$ zu $\ket{D,1}$ und wir haben den Zustand $\ket{S}$ vom Ion I auf die Vibrationsmode übertragen. Dass sich dabei der Zustand des Ions ändert, stellt kein Problem dar.
\begin{figure}[h!tp]
	\centering
	\includegraphics[width=.3\textwidth]{Bilder/swap.pdf}	
	\caption{Swap-Operation \cite{vortragZoller}}
	\label{fig:swap}
\end{figure}

\subsubsection{CNOT (Vibrationsmode $\rightarrow$ Target)}

Das eigentliche CNOT-Gatter besteht insgesamt aus einen Phasengatter und jeweils einem Ramsey-Puls. Das Phasengatter ist dabei ein $2 \pi$-Puls auf dem blauen Seitenband, wie in \reffig{fig:phase}. Der $2 \pi$-Puls besteht noch einmal aus 4 einzelnen Pulsen, die uns aber hier nicht weiter interessieren sollen. Bei dieser Drehung um $2 \pi$, vom Startzustand wieder in den Startzustand zurück, entsteht ein Phasenfaktor von $-1$. Also z.B.
\begin{align}
	\ket{S,0} \rightarrow - \ket{S,0} \; .
\end{align}
Die $2 \pi$-Drehung wirkt auf alle Zustände außer $\ket{D,0}$. Die verursachte Phase ist jedoch eine globale Phase und somit nicht beobachtbar. Dies ist auch in \reffig{fig:4puls} zu sehen. Dort wird das Phasengatter auf den Zustand $\ket{S,0}$ angewendet.
\begin{figure}[h!tp]
	\centering
	\includegraphics[width=.3\textwidth]{Bilder/phase.pdf}	
	\caption{$2\pi$-Puls auf blauem Seitband \cite{vortragZoller}}
	\label{fig:phase}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=.6\textwidth]{Bilder/4puls.pdf}
	\caption{Wirkung des $2 \pi$-Puls auf dem rechten Seitenband mit Startzustand $\ket{S,0}$ \cite{vortragZoller}}
	\label{fig:4puls}
\end{figure}

Um die globale Phase sichtbar zu machen und einen Nutzen daraus zu ziehen, wendet man vor und nach dem Phasengatter einen $\pm \frac{\pi}{2}$-Puls (Ramsey) an. Dieser Ramsey-Puls entspricht einem Hadamard-Gatter und besitzt folgende Wirkung:
\begin{align}
	R_n (\pi/2) : S &\rightarrow \frac{1}{\sqrt{2}} (S - D) \\ D &\rightarrow \frac{1}{\sqrt{2}} (S + D) \\
	R_n (-\pi/2) : S &\rightarrow \frac{1}{\sqrt{2}} (S + D) \\ D &\rightarrow \frac{1}{\sqrt{2}} (D - S)
\end{align}
Diese Drehung ist sensitiv auf den Phasenfaktor und invertiert so den Zustand. Dies ist in \reffig{fig:6puls} deutlich zu sehen. Damit haben wir genau das Verhalten eines CNOT-Gatters erreicht.
\begin{figure}
	\centering
	\includegraphics[width=.6\textwidth]{Bilder/4puls2.pdf}
	\caption{CNOT-Gatter für ein Ion mit Startzustand $\ket{S,0}$ \cite{vortragZoller}}
	\label{fig:6puls}
\end{figure}

\subsubsection{$\text{SWAP}^{-1}$ (Vibrationsmode $\rightarrow$ Control)}

Der $\text{SWAP}^{-1}$-Puls ist wieder ein $\pi$-Puls und dreht den Zustand $\ket{D,1}$ im Control-Bit zurück in den Zustand $\ket{S,0}$. Dies ist in \reffig{fig:swap-1} zu sehen.
	\begin{figure}[h!tp]
		\centering
		\includegraphics[width=.3\textwidth]{Bilder/swap-1.pdf}	
		\caption{$\text{SWAP}^{-1}$-Operation \cite{vortragZoller}}
		\label{fig:swap-1}
	\end{figure}
	
\newpage
	
\subsection{Wirkung der Pulssequenz}

Nun wollen wir uns die Wirkung der Pulssequenz klarmachen. Dazu betrachten wir den Startzustand $\ket{D,S,0}$ und wenden zunächst das SWAP-Gatter auf das Ion I an (\refeq{eq:pulswirk1}). Da der SWAP-Puls nur auf den Zustand $\ket{S,0}$ und nicht auf $\ket{D,0}$ wirkt, bleibt der Zustand des Systems hier konstant. \\
Als nächstes wenden wir den $\frac{\pi}{2}$-Puls (Ramsey) auf das Ion II an, was den Zustand $\ket{S}$ in $\frac{1}{\sqrt{2}}  \ket{S+D}$ überführt (\refeq{eq:pulswirk2}). \\
Der nächste Puls ist der $2 \pi$-Puls auf Ion II, was bei allen Zuständen außer $\ket{D,0}$ einen Phasenfaktor von $-1$ bewirkt und der neue Zustand des Ions II ist $\frac{1}{\sqrt{2}}  \ket{-S+D}$ (\refeq{eq:pulswirk3}). \\
Dann müssen wir den $- \frac{\pi}{2}$-Puls auf Ion II wirken lassen und sehen, dass sich der Zustand nun auf $\ket{D}$ geändert hat (\refeq{eq:pulswirk4}). \\
Als letzten Schritt übertragen wir den Zustand der Vibrationsmode durch den $\text{SWAP}^{-1}$-Puls wieder auf Ion I, was in diesem Beispiel keine Änderung am Gesamtzustand bewirkt (\refeq{eq:pulswirk5}). \\
Wir sehen also, dass sich der Startzustand $\ket{D,S,0}$, wie bei einem CNOT-Gatter gewünscht, zu $\ket{D,D,0}$ geändert hat.

\begin{align}
 \ket{D,S,0} \quad &\underrightarrow{\text{SWAP}} \quad \quad \quad \; \; \ket{D,S,0} \label{eq:pulswirk1} \\
 &\underrightarrow{\text{Ramsey}} \quad \frac{1}{\sqrt{2}} \; \ket{D,S+D,0} \label{eq:pulswirk2} \\
 &\underrightarrow{\text{Phase}}  \quad \quad \frac{1}{\sqrt{2}} \; \ket{D,-S+D,0} \label{eq:pulswirk3} \\
 &\underrightarrow{\text{Ramsey}} \quad \quad \frac{1}{2} \; \ket{D,-S+D+D+S,0} = \ket{D,D,0} \label{eq:pulswirk4} \\
 &\underrightarrow{\text{SWAP}^{-1}} \quad \quad \; \; \; \, \ket{D,D,0} \label{eq:pulswirk5}
\end{align}

Dieselben Schritte für alle mögliche Anfangszustände sind in \reftab{tab:wirk} zusammengefasst. Dabei wurde der Faktor $\frac{1}{\sqrt{2}}$ vernachlässigt.
\begin{table}[h!tp]
	\centering
\begin{tabular}{c|c|c|c|c|c}
   & SWAP & Ramsey & Phase & Ramsey & SWAP \\ \hline
  $\ket{S,S,0}$ & $\ket{D,S,1}$ & $\ket{D,S+D,1}$ & $- \ket{D,S+D,1}$ & $- \ket{D,S,1}$ & $- \ket{S,S,0}$ \\ \hline
	$\ket{S,D,0}$ & $\ket{D,D,1}$ & $\ket{D,D-S,1}$ & $-\ket{D,D-S,1}$ & $- \ket{D,D,1}$ & $-\ket{S,D,0}$ \\ \hline
	$\ket{D,S,0}$ & $\ket{D,S,0}$ & $\ket{D,S+D,0}$ & $\ket{D,-S+D,0}$ & $\ket{D,D,0}$ & $\ket{D,D,0}$ \\ \hline
	$\ket{D,D,0}$ & $\ket{D,D,0}$ & $\ket{D,D-S,0}$ & $\ket{D,D+S,0}$ & $\ket{D,S,0}$ & $\ket{D,S,0}$ \\ \hline
 \end{tabular}
 \caption{Zusammenfassung des CNOT-Gatter von Cirac-Zoller}
 \label{tab:wirk}
 \end{table}
Vergleichen wir dies mit der gewünschten Wirkung aus \reftab{tab:types}, sehen wir, dass dies, bis auf einen gelegentlichen Phasenfaktor, einem CNOT-Gatter entspricht. Die Phase $-1$ ist eine globale Phase und somit nicht messbar. Wir können also durch ein Phasengatter und jeweils einem Ramsey-Puls davor und danach ein CNOT-Gatter implementieren. \\
Die Wirkung ist auch in \reffig{fig:wirk} graphisch zu sehen. Der Startzustand wird darin zum Zeitpunkt $t = \SI{0}{\second}$ definiert und nicht durch den Startpunkt der jeweiligen Graphik.
\begin{figure}[h!tp]
	\centering
\includegraphics[width=.75\textwidth]{Bilder/cnot.pdf}	 
\caption{Zusammenfassung des CNOT-Gatter \cite{vortragZoller}}
\label{fig:wirk}
\end{figure}

\newpage

\section{Dipol-Dipol-Wechselwirkungs-Gatter für neutrale Atome}
\label{sec:dd}

Eine weitere mögliche Realisierung eines Quantengatters basiert auf der Dipol-Dipol-Wechselwirkung zwischen zwei Atomen. Man fängt dazu zwei Atome in einem optischen Gitter und induziert, abhängig vom Zustand des Atoms, starke Dipole. Deutsch et al. \cite{Deutsch2000Quantum-Computi} verwendeten dazu die Zustände $\ket{S_{1/2},F_{\uparrow}}$ und $\ket{S_{1/2},F_{\downarrow}}$ als Qubitzustand $\ket{0}$ und $\ket{1}$. Der Laser wird auf den Übergang $\ket{S_{1/2},F_{\uparrow}} \rightarrow \ket{P_{3/2}}$ eingestellt, sodass nur bei einem Zustand $\ket{1}$ ein Dipol induziert wird.
\begin{figure}[h!tp]
	\centering
	\includegraphics[height=.3\textheight]{Bilder/cphase.pdf}	
	\caption{Energie-Level-Struktur der logischen Basis \cite{Deutsch2000Quantum-Computi}}
	\label{fig:ddww}
\end{figure}
Bringt man nun die zwei Atome für eine gewisse Zeit $t$ nahe genug zusammen, verursacht die Dipol-Dipol-Wechselwirkung eine Energieverschiebung $\Delta E(t) = V_{dd}$, welche wiederum eine Phase $\phi \propto \integral{t} \Delta E(t)$ induziert. Aufgrund der Wahl der Laserfrequenz passiert dies nur bei dem Zustand 
\begin{align}
	\ket{1,1} \rightarrow e^{-i \phi} \ket{1,1} \; .
\end{align}
Wendet man nun noch vor und nach dieser Wechselwirkung einen Ramsey-Puls auf das System an, wirkt diese Sequenz als ein CNOT-Gatter.

\section{Gatter basierend auf der Resonator-Atom-Wechselwirkung}
\label{sec:hohl}

Als Grundlage für ein Quantengatter ist auch eine Resonator-Atom-Interaktion denkbar. Dazu werden Vakuum-Rabi-Oszillationen zwischen einem Atom und dem Resonator verwendet. Die Idee ist, ähnlich wie dem Zwischenspeichern in der Vibrationsmode, den Zustand des ersten Atoms im Resonator durch einen 0 oder 1-Photon-Zustand zu speichern und dann abhängig davon den Zustand des zweiten Atoms zu verändern. \cite{Raimond2001Colloquium:-Man} \\
Dazu schießt man das erste Atom mit den Grundzustand $\ket{g}$ und dem angeregten Zustand $\ket{e}$, durch Resonator und wendet einen $\pi$-Puls auf das System an. Dadurch wird das System nach
\begin{align}
	\ket{e,0} \; &\rightarrow \; \ket{g,1} \\ \nonumber
	\ket{g,1} \; &\rightarrow \; -\ket{e,0}
\end{align}
verändert. Nimmt man nun ein zweites Atom, dessen angeregter Zustand soweit von der Resonanz mit dem Resonator verschoben ist, dass der Resonator ein Atom in diesem Zustand $\ket{i}$ nicht sieht, und wendet einen $2 \pi$-Puls darauf an, so ändert sich der Zustand nach \refeq{eq:hohl}. Ist kein Photon im Resonator vorhanden, d.h. er ist im Zustand $\ket{0}$, ändert sich der Zustand des Atoms nicht.
\begin{align}
	(c_g \ket{g} + c_i \ket{i}) \, \ket{1} \rightarrow \; (- c_g \ket{g} + c_i \ket{i}) \, \ket{1}
	\label{eq:hohl}
\end{align}
Durch diese Wechselwirkung haben wir, wie beim Cirac-Zoller-Gatter, ein Phasengatter realisiert und können durch zwei Ramsey-Pulse das CNOT-Gatter implementieren.

\begin{figure}[h!tp]
	\centering
	\includegraphics[width=.5\textwidth]{Bilder/rabi.pdf}	
	\caption{Vakuum-Rabi-Oszillation \cite{Raimond2001Colloquium:-Man}}
	\label{fig:rabi}
\end{figure}


\section{Gatter durch kontrollierte Kollisionen}
\label{sec:koll}

Will man ein Gatter durch kontrollierte Kollisionen realisieren, müssen zunächst wieder Atome in Potentialen gefangen werden. Diese Potentialtöpfe müssen einzeln bewegt werden können. Wählt man nun die Trajektorien der Fallen derart, dass sie je nach internem Zustand des jeweiligen Atoms in eine andere Richtung laufen, so lässt sich eine kontrollierte Kollision bei einer bestimmen Konfiguration herbeiführen. Die Trajektorien sind in \reffig{fig:koll} dargestellt.\\
\begin{figure}[h!tp]
	\centering
	\includegraphics[width=.6\textwidth]{Bilder/kollisionen.pdf}	
	\caption{Konfiguration zu den Zeiten $\tau$ (a) \newline und $t$ (b). $\tau$ ist dabei der Startzeitpunkt. \cite{Jaksch1999Entanglement-of}}
	\label{fig:koll}
\end{figure}
Zum Zeitpunkt $\tau$ befinden sich die Atome an ihrem Startpunkt. Befindet sich das Atom im Zustand $\ket{b}$ bzw. $\ket{a}$, bewegt es sich nach links bzw. rechts. Nur für den Fall, dass sich Atom 1 in $\ket{a}$ und Atom 2 in $\ket{b}$ befindet, findet eine Kollision der Atome statt. Diese Kollision bewirkt eine Verschiebung der Energieniveaus und somit eine Phase $\phi^{ab}$. Die Bewegung der Potentiale verursacht je nach Zustand ebenfalls eine Phase $\phi^{a}$ bzw. $\phi^{b}$. Insgesamt ergibt sich also die Wirkung
\begin{align}
	\ket{a,a} &\rightarrow e^{-i2\phi^a} \ket{a,a} \\ \nonumber
	\ket{a,b} &\rightarrow e^{-i (\phi^a+\phi^b+\phi^{ab})} \ket{a,b} \\ \nonumber
	\ket{b,a} &\rightarrow e^{-i(\phi^a+\phi^b)} \ket{b,a} \\ \nonumber
	\ket{b,b} &\rightarrow e^{-i2\phi^b} \ket{b,b}			\; .
\end{align}
Wählt man nun die Zeiten geschickt, lassen sich die Phasen bestimmen und somit die genaue Wirkung des Gatters. Mit Ramsey-Pulsen lässt sich so wieder das CNOT-Gatter realisieren.


\chapter{Zusammenfassung und Ausblick}

Wir haben gesehen, dass sich ein Quantengatter mit den unterschiedlichsten Wechselwirkungen realisieren lässt. Zum Einen durch direkte Wechselwirkung der beteiligten Qubits wie beispielsweise bei den kontrollierten Kollisionen in Abschnitt~\ref{sec:koll} oder der Dipol-Dipol-Wechselwirkung aus Abschnitt~\ref{sec:dd}. Auch indirekte Wechselwirkungen wie bei dem Cirac-Zoller-Gatter in Abschnitt~\ref{sec:cirac} oder der Resonator-Atom-Interaktion in Abschnitt~\ref{sec:hohl} sind denkbar und wurden schon realisiert. Es ist auch möglich Quantengatter sowohl für neutrale Atome, als auch für Ionen zu implementieren. \\
Allerdings gibt es noch einige offene Fragen. Die Skalierbarkeit ist nicht bei jeder der genannten Möglichkeiten ohne weiteres gegeben. Bei den kontrollierten Kollisionen wäre es notwendig, die zwei gewünschten Atome durch das gesamte Gitter zu bewegen und dann kollidieren zu lassen, was erhelbliche Schwierigkeiten bereitet. Daher wurde dieses Gatter bisher nur mit zwei Atomen realisiert. Das Cirac-Zoller-Gatter hingegen ist sehr gut skalierbar, da der Informationsaustausch über die gemeinsame Vibrationsmode aller Ionen funktioniert und der genaue Ort der zwei Qubits somit keine Rolle spielt. \\
Desweiteren stellt sich die Frage nach der Performance der einzelnen Quantengatter. Zwar lässt sich durch die genannten Möglichkeiten ein universelles Set von Gattern realisieren und somit auch alle möglichen Quantengatter. Allerdings muss dies nicht die schnellste Möglichkeit sein und man muss gegenenfalls sich eine neue Art der Realisierung des Quantengatters überlegen.


\bibliographystyle{natdin}
\large
\bibliography{bibliothek}
\nocite{*}

\end{document}
